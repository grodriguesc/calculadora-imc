import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Imc",
      home: Imc(),
    );
  }
}

class Imc extends StatefulWidget {
  @override
  _ImcState createState() => _ImcState();
}

class _ImcState extends State<Imc> {
  final _pesoController = TextEditingController();
  final _alturaController = TextEditingController();

  String _infoText = '';

  String asjdhaskdjhaskjd;
  bool sajkhdjksa;

  Color _cor;
  conflitooooooooo;
  double _peso;
  double _altura;
  double _imc;

  void _calcularIMC() {
    _peso = double.parse(_pesoController.text);
    _altura = double.parse(_alturaController.text);

    _imc = (_peso) / (_altura * _altura);

    if (_imc > 40) {
      _imcTipo = "obesidade morbida!";
      _cor = Colors.red;
    } else if (_imc > 30) {
      _imcTipo = "obesidade!";
      _cor = Colors.red[400];
    } else if (_imc > 25) {
      _imcTipo = "sobrepeso!";
      _cor = Colors.red[300];
    } else if (_imc > 20) {
      _imcTipo = "o peso ideal!";
      _cor = Colors.green;
    } else if (_imc > 16) {
      _imcTipo = "subpeso!";
      _cor = Colors.red[300];
    } else if (_imc < 16) {
      _imcTipo = "sobrepeso severo!";
      _cor = Colors.red;
    }

    _imcString = _imc.toStringAsFixed(1);

    _infoText = "Seu imc é $_imcString você está com $_imcTipo";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: Text("Calculadora de IMC",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ))),
      body: Container(
        color: Colors.teal,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Text(
                "Digite seu peso:",
                style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
              ),
            ),
            TextField(
                controller: _pesoController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'Peso em Kg'),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(10),
                ]),
            Center(
              child: Text(
                "Digite sua altura:",
                style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
              ),
            ),
            TextField(
                controller: _alturaController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'Altura em m'),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(10),
                ]),
            RaisedButton(
              color: Colors.blueGrey,
              onPressed: () {
                _calcularIMC();
              },
              child: Text(
                "Calcular IMC",
                style: TextStyle(color: Colors.white),
              ),
            ),
            Center(
              child: Text(
                "$_infoText",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 26, fontWeight: FontWeight.bold, color: _cor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
